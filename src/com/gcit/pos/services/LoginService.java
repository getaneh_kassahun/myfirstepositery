package com.gcit.pos.services;

import com.gcit_pos_hibernate.DAO.TicketDAO;
import com.gcit_pos_hibernate.DAO.UserTypeDAO;
import com.gcit_pos_hibernate.DAO.UsersDAO;
import com.gcit_pos_hibernate.entities.Users;

public class LoginService {

	private UsersDAO userDAO;
	private UserTypeDAO baseUserTypeDao;
	private TicketDAO ticketDao;

	public boolean doLogin(Users user) throws Exception {

		boolean result = false;

		try {
			Users userfromdo = null;
			user.setN_USER_TYPE(1);
			
			userfromdo = (Users) userDAO.retrieve(user);

			if (userfromdo != null && userfromdo.getUSER_ID() != 0
					&& user.getUSER_ID() == userfromdo.getUSER_ID()) {
				if (userfromdo.getUSER_PASS() != null && 
						user.getUSER_PASS().equalsIgnoreCase(
								userfromdo.getUSER_PASS())) {
					if (userfromdo.getN_USER_TYPE() != 0 && 
							user.getN_USER_TYPE() == userfromdo.getN_USER_TYPE()) {
						result = true;
					} else {
						throw new Exception("The user's role is not valid!");
					}
				} else {
					throw new Exception("The user's password is not valid!");
				}				
			} else {
				throw new Exception("The User Id is not valid!");
			}

		} catch (Exception e) {
			throw e;
		}

		return result;

	}

	public TicketDAO getTicketDao() {
		return ticketDao;
	}

	public void setTicketDao(TicketDAO ticketDao) {
		this.ticketDao = ticketDao;
	}

	public UserTypeDAO getBaseUserTypeDao() {

		return baseUserTypeDao;
	}

	public void setBaseUserTypeDao(UserTypeDAO baseUserTypeDao) {
		this.baseUserTypeDao = baseUserTypeDao;

	}

	public UsersDAO getUserDAO() {
		return userDAO;
	}

	public void setUserDAO(UsersDAO userDAO) {
		this.userDAO = userDAO;
	}

}
